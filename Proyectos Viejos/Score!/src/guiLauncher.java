import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import java.awt.Point;
import javax.swing.JOptionPane;


public class guiLauncher extends Dialog {

	protected Object result;
	protected Shell shell;
	private Text angleIn;
	private Text forceIn;
	private Combo listaLanzables;
	private Label xR;
	private Label yR;
	private Text gIn;
	private Text timeIn;

	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public guiLauncher(Shell parent, int style) {
		super(parent, style);
		setText("SWT Dialog");
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open(String lista) {
		createContents(lista);
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents(String lista) {
		shell = new Shell(getParent(), getStyle());
		shell.setSize(450, 469);
		shell.setText(getText());
		
		Button btnBack = new Button(shell, SWT.NONE);
		btnBack.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				shell.close();
				shell.dispose();
			}
		});
		btnBack.setBounds(44, 254, 75, 25);
		btnBack.setText("Back");
		
		listaLanzables = new Combo(shell, SWT.NONE);
		listaLanzables.setBounds(44, 52, 91, 23);
		
		String[] elements=lista.split("\n");
		for(int i=0 ; i<elements.length ; i++)
		{
			listaLanzables.add(elements[i]);

		}
		
		
		Label lblBall = new Label(shell, SWT.NONE);
		lblBall.setBounds(44, 20, 55, 15);
		lblBall.setText("Ball");
		
		Label lblAngleinRadians = new Label(shell, SWT.NONE);
		lblAngleinRadians.setBounds(31, 111, 118, 15);
		lblAngleinRadians.setText("Angle(In radians , DA)");
		
		angleIn = new Text(shell, SWT.BORDER);
		angleIn.setBounds(44, 147, 76, 21);
		
		Label lblForce = new Label(shell, SWT.NONE);
		lblForce.setBounds(188, 20, 135, 15);
		lblForce.setText("Force( In Newtons , DA) ");
		
		forceIn = new Text(shell, SWT.BORDER);
		forceIn.setBounds(188, 52, 76, 21);
		
		Label lblX = new Label(shell, SWT.NONE);
		lblX.setBounds(42, 349, 8, 15);
		lblX.setText("x:");
		
		xR = new Label(shell, SWT.NONE);
		xR.setBounds(75, 349, 99, 15);
		
		Label lblY = new Label(shell, SWT.NONE);
		lblY.setBounds(42, 391, 8, 15);
		lblY.setText("y:");
		
		yR = new Label(shell, SWT.NONE);
		yR.setBounds(75, 391, 99, 15);
		
		Button btnLaunch = new Button(shell, SWT.NONE);
		btnLaunch.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int   time=0  ;
				double angle=0 , force=0 , g=0;
				try
				{
					angle=Double.parseDouble(angleIn.getText());
					force=Double.parseDouble(forceIn.getText());
					time=Integer.parseInt(timeIn.getText());
					g=Double.parseDouble(gIn.getText());
				}
				catch(Exception exc)
				{
					
					JOptionPane.showMessageDialog(null, "Error in parsing the values");
					return;
				}
				String name=listaLanzables.getText().substring(0 , (listaLanzables.getText().indexOf('(')-1));
				System.out.println(name);
				Ball ball = Game.instanceGame().ballsFabric.findBall(name);
				System.out.println(ball.name);
				Point point=Game.instanceGame().launchBall(ball, angle, force , time , g);
				System.out.println("x:"+point.x+" | y:"+point.y);
				xR.setText(point.x+"");
				yR.setText(point.y+"");
				
				guiMovimiento temp = new guiMovimiento(shell,0);
				temp.open();
			}
		});
		btnLaunch.setBounds(189, 254, 75, 25);
		btnLaunch.setText("Launch");
		
		Label lblResults = new Label(shell, SWT.NONE);
		lblResults.setBounds(135, 309, 55, 15);
		lblResults.setText("Results");
		
		gIn = new Text(shell, SWT.BORDER);
		gIn.setBounds(188, 147, 76, 21);
		
		Label lblGravedad = new Label(shell, SWT.NONE);
		lblGravedad.setBounds(188, 111, 135, 15);
		lblGravedad.setText("Gravedad(In m/s^2 , DA)");
		
		Label lblTime = new Label(shell, SWT.NONE);
		lblTime.setBounds(109, 181, 99, 15);
		lblTime.setText("Time(In seconds)");
		
		timeIn = new Text(shell, SWT.BORDER);
		timeIn.setBounds(122, 209, 76, 21);
		
		Label lblDaDecimalAllowed = new Label(shell, SWT.NONE);
		lblDaDecimalAllowed.setBounds(280, 209, 113, 15);
		lblDaDecimalAllowed.setText("DA= decimal allowed");
		
		
		
		
		
	}
}

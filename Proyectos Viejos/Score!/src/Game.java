import java.awt.Point;

//Aqui es donde se lleva a cabo la simulacion del juego
// And there is one game and only one.
public class Game {
	private static Game instanceGame;
	public BallsFabric ballsFabric;
	public Simulation simulation;
	public Gui gui;
	
	
	
	private Game() {
		ballsFabric = new BallsFabric();
		simulation = new Simulation();
		
	}
	public static synchronized Game instanceGame()
	{
		if( instanceGame==null)
		{
			instanceGame=new Game();
		}
	
		return instanceGame;
	}
	public void setGui(Gui gui) {
		
		this.gui=gui;
	}
	public Point launchBall(Ball ball , double angle , double force , int tiempo , double gravedad)
	{
		int weight=ball.weight;
		Point point=ball.position;
		System.out.println("In sim ball x:"+ball.position.x+" | y:"+ball.position.y);
		System.out.println("angulo "+angle);
		double ax=simulation.startingAX(angle, force, weight);
		double ay=simulation.startingAY(angle, force, weight);
		System.out.println("ax: "+ax+" | ay"+ay);
		double sx=0;
		double sy=0;
		int max=100000;
		while(true)
		{
			/*if(point.y-sy<0 || max==0 || tiempo==0)
			{
				point.x+=sx-(sy-point.y);
				break;
			}*/
			if(max==0 || tiempo==0)
			{
				break;
			}
			point.x+=sx;
			point.y+=sy;
			sx+=ax;
			sy+=ay-gravedad;
			max--;
			tiempo--;
		
		}
		System.out.println("In sim x:"+point.x+" | y:"+point.y);
		ball.position=point;
		return point;
		
	}

}

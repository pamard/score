import java.awt.*;

//esta clase permite crear objetos con las características pertinentes.
public class MyObject {

	public double mass;
	public int gravity;
	public int velocity;
	public int angle;
	public Point position;
	public Point inicialPosition;
	
	public MyObject(double m, int g, int v, int a, Point p)
	{
		mass = m;
		gravity = g;
		velocity = v;
		angle = a;
		position = p;
		inicialPosition = p;
		
	}
	
	/**
	 * método que permite crear un informe con las características del objeto al momento de llamarlo.
	 * @return un string con el informe deseado
	 */
	public String informeCaracteristicas()
	{
		return "<html><pre>Características:"+
	"\nÁngulo: "+angle+" grados"+
	"\nVelocidad: "+velocity+" m/s²"+
	"\nGravedad: "+gravity+" m/s²"+
	"\nMasa: "+mass+" kg"+
	"\nPosición: ("+((int)position.getX()+", "+(int)position.getY())+ ")</pre></html>";
		
	}
	
	
	
}

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

//esta clase representa la ventana que permite elegir el mundo en el que se jugará (modo competitivo).
public class SwingElegirMundo extends Gui{

	public JPanel myPanel;
	public GridBagConstraints gbc;
	private JButton mundoMercurio;
	private JButton mundoTierra;
	private JButton mundoJupiter;
	
	public SwingElegirMundo()
	{
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		myPanel = new JPanel();
		myPanel.setLayout(new GridBagLayout());
		gbc = new GridBagConstraints();
		
		JLabel labelInstruccion = new JLabel("<html><pre>Haga click en el mundo en el que desea jugar:</pre></html>");
		gbc.gridy = 0;
		gbc.gridx = 1;
		gbc.insets = new Insets(10, 10, 10, 10);
		myPanel.add(labelInstruccion, gbc);
		
		
		//imagen, botón y etiqueta para el mundo Mercurio
		ImageIcon imagenMercurio = new ImageIcon("Imagenes\\Mercurio.jpg");
		Image img = imagenMercurio.getImage();  
		Image newimg = img.getScaledInstance(300, 400,  java.awt.Image.SCALE_SMOOTH);  
		imagenMercurio = new ImageIcon(newimg);
		
		mundoMercurio = new JButton(imagenMercurio);
		mundoMercurio.addActionListener(this);
		gbc.gridy = 1;
		gbc.gridx = 0;
		myPanel.add(mundoMercurio, gbc);
		
		JLabel labelMercurio = new JLabel("<html><pre>\tMercurio\n Gravedad: 3 m/s²</pre></html>");
		gbc.gridy = 2;
		gbc.gridx = 0;
		gbc.insets = new Insets(10, 10, 10, 10);
		myPanel.add(labelMercurio, gbc);
		
		
		//imagen, botón y etiqueta para el mundo Tierra
		ImageIcon imagenTierra = new ImageIcon("Imagenes\\Tierra.jpg");
		img = imagenTierra.getImage();  
		newimg = img.getScaledInstance(300, 400,  java.awt.Image.SCALE_SMOOTH);  
		imagenTierra = new ImageIcon(newimg);
		
		mundoTierra = new JButton(imagenTierra); 
		mundoTierra.addActionListener(this);
		gbc.gridy = 1;
		gbc.gridx = 1;
		myPanel.add(mundoTierra, gbc);
		
		JLabel labelTierra = new JLabel("<html><pre>\tTierra\n Gravedad: 10 m/s²</pre></html>");
		gbc.gridy = 2;
		gbc.gridx = 1;
		gbc.insets = new Insets(10, 10, 10, 10);
		myPanel.add(labelTierra, gbc);
		
		
		//imagen, botón y etiqueta para el mundo Jupiter
		ImageIcon imagenJupiter = new ImageIcon("Imagenes\\Jupiter.jpg");
		img = imagenJupiter.getImage();  
		newimg = img.getScaledInstance(300, 400,  java.awt.Image.SCALE_SMOOTH);  
		imagenJupiter = new ImageIcon(newimg);
		
		mundoJupiter = new JButton(imagenJupiter); 
		mundoJupiter.addActionListener(this);
		gbc.gridy = 1;
		gbc.gridx = 2;
		myPanel.add(mundoJupiter, gbc);
	
		JLabel labelJupiter = new JLabel("<html><pre>\tJupiter\n  Gravedad: 23 m/s²</pre></html>");
		gbc.gridy = 2;
		gbc.gridx = 2;
		gbc.insets = new Insets(10, 10, 10, 10);
		myPanel.add(labelJupiter, gbc);
		
		
		this.add(myPanel);
	}
	
	@Override
	public void actionPerformed(ActionEvent event){
		
		if(event.getSource() == mundoMercurio)
		{
			SwingJuego modoPrueba = new SwingJuego(false, 1);
			modoPrueba.setTitle("SCORE - MODO COMPETITIVO (MERCURIO)");
			modoPrueba.setSize(750, 450);
			modoPrueba.centreWindow();
			modoPrueba.setVisible(true);
		}
		
		if(event.getSource() == mundoTierra)
		{
			SwingJuego modoPrueba = new SwingJuego(false, 2);
			modoPrueba.setTitle("SCORE - MODO COMPETITIVO (TIERRA)");
			modoPrueba.setSize(750, 450);
			modoPrueba.centreWindow();
			modoPrueba.setVisible(true);
		}
		
		if(event.getSource() == mundoJupiter)
		{
			SwingJuego modoPrueba = new SwingJuego(false, 3);
			modoPrueba.setTitle("SCORE - MODO COMPETITIVO (JUPITER)");
			modoPrueba.setSize(750, 450);
			modoPrueba.centreWindow();
			modoPrueba.setVisible(true);
		}
		
		this.dispose();
	}
	
	
	
	
}

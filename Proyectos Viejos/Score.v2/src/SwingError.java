import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

//Ventana que se abre cuando hay error de formato en la ventana Modificar
public class SwingError extends Gui
{
	private JPanel panel = new JPanel();
	JLabel labelMensaje;
	
	public SwingError(String msje)
	{
		labelMensaje = new JLabel(msje);
		
		panel.setLayout(new GridBagLayout());
        panel.add(labelMensaje);
        
		this.setTitle("Error");
		this.setSize(600, 100);
		this.centreWindow();
		setResizable( false );
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		/*myPanel = new JPanel();
		myPanel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelMensaje, gbc);*/
		this.add(panel);
		//this.add(boton);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent event)
	{
		this.dispose();
		
	}
	
	

}

import java.awt.Point;


public class Obstaculo {
	
	public Point position;
	public int tamano;
	
	public Obstaculo(int x, int y, int tamano)
	{
		position = new Point(x,y);
		this.tamano = tamano;
	}

}

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;

import javax.swing.*;

//esta clase representa la ventana del juego. En ella, se pueden cambiar las características del objeto y lanzar.
public class SwingJuego extends Gui {

	public JButton BotonModificar;
	public JButton BotonLanzar;
	public JButton BotonVolver;
	public MyObject objeto;
	public Goal meta;
	public Obstaculo[] obstaculos; 
	public int numObstac;
	public JPanel myPanel;
	public boolean modoPrueba;
	public int mundo;
	public SwingModificar SM;
	JLabel caracteristicasObjeto;
	GridBagConstraints gbc;
	
	public SwingJuego(boolean modo, int numMundo)
	{
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		modoPrueba = modo;
		mundo = numMundo;
		myPanel = new JPanel();
		myPanel.setLayout(new GridBagLayout());
		gbc = new GridBagConstraints();
		
		//se crea y ubica un grilla en la ventana con el fin de poder simular el desplazamiento del objeto de forma gráfica.
		Grilla temp=new Grilla();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 10;
		gbc.insets = new Insets(25, 50, 50, 0);
		Point pos = new Point(0,0);// Mirar esto, esta deberia ser la posision de la pelota, no 0,0
		myPanel.add(temp.getGrilla(pos), gbc);
		
		
		//a continuación se va a crear y ubicar un fondo para el desplazamiento de la pelota.
		JLabel pastoCancha;
		
		if(modoPrueba)
		{
			objeto = new MyObject(1, 10, 10, 45, new Point(0,0));
			Point posMeta = new Point(20,20);
			meta = new Goal(posMeta);
			numObstac=0;
			pastoCancha = new JLabel(new ImageIcon("Imagenes\\sky.jpg"));
		}
		
		//en caso de ser modo competitivo y según el mundo escogido, se cambian algunas características del juego.
		else
		{
			ImageIcon imagenSuperficie;
			
			//mundo Mercurio, imagen de superficie específica, se fija la meta en (60, 40) y se crean 2 obstáculos.
			if(mundo == 1)
			{
				objeto = new MyObject(1, 3, 10, 45, new Point(0,0));
				Point posMeta = new Point(60,40);
				meta = new Goal(posMeta);
				numObstac=2;
				obstaculos = new Obstaculo[2];
				obstaculos[0] = new Obstaculo(5,5,1);
				Map.instanceMap().setObstacle(obstaculos[0]);
				obstaculos[1] = new Obstaculo(10,8,1);
				Map.instanceMap().setObstacle(obstaculos[1]);
				imagenSuperficie = new ImageIcon("Imagenes\\SuperficieMercurio2.jpg");
			}
			
			//mundo Tierra, imagen de superficie específica, se fija la meta en (80, 20) y se crean 3 obstáculos.
			else if(mundo == 2)
				{
					objeto = new MyObject(1, 10, 10, 45, new Point(0,0));
					Point posMeta = new Point(80,20);
					meta = new Goal(posMeta);
					numObstac=3;
					obstaculos = new Obstaculo[3];
					obstaculos[0] = new Obstaculo(6,7,1);
					Map.instanceMap().setObstacle(obstaculos[0]);
					obstaculos[1] = new Obstaculo(4,5,1);
					Map.instanceMap().setObstacle(obstaculos[1]);
					obstaculos[2] = new Obstaculo(15,8,1);
					Map.instanceMap().setObstacle(obstaculos[2]);
					imagenSuperficie = new ImageIcon("Imagenes\\SuperficieTierra.jpg");
				}
				 
				//mundo Jupiter, imagen de superficie específica, se fija la meta en (50, 60) y se crean 3 obstáculos.
				else 
				{
					objeto = new MyObject(1, 23, 10, 45, new Point(0,0));
					Point posMeta = new Point(50,60);
					meta = new Goal(posMeta);
					numObstac=3;
					obstaculos = new Obstaculo[3];
					obstaculos[0] = new Obstaculo(4,4,1);
					Map.instanceMap().setObstacle(obstaculos[0]);
					obstaculos[1] = new Obstaculo(10,3,1);
					Map.instanceMap().setObstacle(obstaculos[1]);
					obstaculos[2] = new Obstaculo(16,15,1);
					Map.instanceMap().setObstacle(obstaculos[2]);
					imagenSuperficie = new ImageIcon("Imagenes\\SuperficieJupiter.jpg");
				}
			
			Image img = imagenSuperficie.getImage();  
			Image newimg = img.getScaledInstance(400, 300,  java.awt.Image.SCALE_SMOOTH);  
			pastoCancha = new JLabel(new ImageIcon(newimg));
		}
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 10;
		gbc.insets = new Insets(25, 50, 50, 0);
		myPanel.add(pastoCancha, gbc);
		
		//se crea la sección en la que se muestran las características del objeto.
		caracteristicasObjeto = new JLabel(objeto.informeCaracteristicas());
		gbc.gridx = 10;
		gbc.gridy = 0;
		gbc.insets = new Insets(0, 50, 0, 50);
		myPanel.add(caracteristicasObjeto, gbc);
		
		//Botón que permite lanzar el objeto.
		BotonLanzar = new JButton("Lanzar");
		BotonLanzar.addActionListener(this);
		gbc.gridx = 0;
		gbc.gridy = 1;
		myPanel.add(BotonLanzar, gbc);
		
		//Botón que permite modificar las características del objeto.
		BotonModificar = new JButton("Modificar");
		BotonModificar.addActionListener(this);
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.anchor = GridBagConstraints.BASELINE_TRAILING;
		myPanel.add(BotonModificar, gbc);
		
		//Bóton que permite volver al menú principal.
		BotonVolver = new JButton("Volver al menú principal");
		BotonVolver.addActionListener(this);
		gbc.gridx = 10;
		gbc.gridy = 1;
		gbc.anchor = GridBagConstraints.BASELINE_TRAILING;
		myPanel.add(BotonVolver, gbc);

		this.add(myPanel);
		
		//Setiando todos los componentes base de la venta SwingModificar
		SM = new SwingModificar(objeto, caracteristicasObjeto, myPanel);
		SM.setTitle("MODIFICAR OBJETO");
		SM.setSize(300, 300);
		SM.SetSJ(this); //Doble dependecia, se pasa la instancia a la ventana hija
		//
		
	}
	
	@Override
	public void actionPerformed(ActionEvent event)
	{
		//en caso de apretar el botón modificar, se abre la ventana correspondiente.
		if(event.getSource() == BotonModificar)
		{
			SM.setVisible(true);
			SM.centreWindow();
		}
		
		//en caso de apretar el botón lanzar, se simula el movimiento del objeto.
		else if (event.getSource() == BotonLanzar)
		{
			int tiempo = 1;
			boolean choco = false;
			while(!choco && ((objeto.position.x > 0 && objeto.position.y > 0) || tiempo == 1))
			{
				int auxX = (int) (objeto.inicialPosition.x + objeto.velocity*Math.cos(Math.toRadians(objeto.angle))*tiempo);
				int auxY = (int) (objeto.inicialPosition.y + objeto.velocity*Math.sin(Math.toRadians(objeto.angle))*tiempo - 0.5 * objeto.gravity*tiempo*tiempo);
				
				System.out.println(auxX);
				System.out.println(auxY);
				
				if(auxX < 0)
				{ 
					auxX = 0;
				}
				
				if(auxY < 0) 
				{ 
					auxY = 0; 
				}
				
				objeto.position = new Point(auxX, auxY);
				
				caracteristicasObjeto.setText(objeto.informeCaracteristicas());
				gbc.gridx = 10;
				gbc.gridy = 0;
				gbc.insets = new Insets(0, 50, 0, 50);
				gbc.anchor = GridBagConstraints.CENTER;
				myPanel.add(caracteristicasObjeto, gbc);
				
				tiempo++;
				System.out.println(objeto.informeCaracteristicas());
				
				if(objeto.position == meta.GetPosition()){
					System.out.println("SCORE!");
					break;
				}
				for(int i = 0; i < numObstac; i++){
					if(objeto.position == obstaculos[i].position){
						System.out.println("Chocaste con un obstáculo!");
						choco = true;
						break;
					}
				}
			}
			
			objeto.inicialPosition = objeto.position;
			System.out.println("listo");
			
		}
		
		//en caso de apretar el botón volver al menú principal, se cierra esta ventana y se abre una del menú principal (SwingElegirModo).
		else
		{
			SwingElegirModo swing = new SwingElegirModo();
			swing.setTitle("BIENVENIDOS");
			swing.setSize(500, 500);
			swing.setVisible(true);
			swing.centreWindow();
			
			this.dispose();
		}
		
	}
	
	/**
	 * Método que actualiza las características del objeto y el JLabel correspondiente en el juego, es llamado por SwingModificar.
	 * @param angle nuevo valor para el ángulo
	 * @param vel nuevo valor para la velocidad
	 * @param mass nuevo valor para la masa
	 * @param gravity nuevo valor para la gravedad
	 * @param posX nuevo valor para la posición en X.
	 * @param posY nuevo valor para la posición en Y.
	 */
	public void UpdateCaract(int angle,int vel,int mass,int gravity,int posX,int posY)
	{
		objeto.mass = mass;
		objeto.gravity = gravity;
		objeto.position = new Point(posX,posY);
		objeto.angle = angle;
		objeto.velocity = vel;
		caracteristicasObjeto.setText(objeto.informeCaracteristicas());
	}
	
	
	
	
	
}

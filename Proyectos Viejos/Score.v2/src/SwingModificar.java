import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

//esta clase representa la ventana que permite modificar características del objeto.
public class SwingModificar extends Gui {

	public MyObject objeto;
	public JPanel PanelVentanaJuego;
	public JLabel txtInforme;
	
	public JPanel myPanel;
	public JButton botonAcceptar;
	public JButton botonCancelar;
	private JTextField txtAngulo;
	private JTextField txtVelocidad;
	private JTextField txtGravedad;
	private JTextField txtMasa;
	private JTextField txtPosX;
	private JTextField txtPosY;
	private SwingJuego SJ; // Se realiza doble contención
	
	
	public SwingModificar(MyObject objeto, JLabel txtInforme, JPanel panel)
	{
		setResizable( false );
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		this.objeto = objeto;
		this.txtInforme = txtInforme;
		this.PanelVentanaJuego = panel;
		
		myPanel = new JPanel();
		myPanel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		//Etiquetas y campo de entrada para modificar el ángulo del objeto.
		JLabel labelAngulo = new JLabel("Angulo:");
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelAngulo, gbc);
		
		txtAngulo = new JTextField(5);
		txtAngulo.setText(""+objeto.angle);
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(txtAngulo, gbc);
		
		JLabel labelAnguloUnidad = new JLabel("grados");
		gbc.gridx = 2;
		gbc.gridy = 0;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelAnguloUnidad, gbc);
		
		//Etiquetas y campo de entrada para modificar la gravedad del objeto.
		JLabel labelGravedad = new JLabel("Gravedad:");
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelGravedad, gbc);
		
		txtGravedad = new JTextField(5);
		txtGravedad.setText(""+objeto.gravity);
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(txtGravedad, gbc);
		
		JLabel labelGravedadUnidad = new JLabel("m/s²");
		gbc.gridx = 2;
		gbc.gridy = 1;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelGravedadUnidad, gbc);
		
		//Etiquetas y campo de entrada para modificar la masa del objeto.
		JLabel labelMasa = new JLabel("Masa:");
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelMasa, gbc);
		
		txtMasa = new JTextField(5);
		txtMasa.setText(""+objeto.mass);
		gbc.gridx = 1;
		gbc.gridy = 2;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(txtMasa, gbc);
		
		JLabel labelMasaUnidad = new JLabel("kg");
		gbc.gridx = 2;
		gbc.gridy = 2;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelMasaUnidad, gbc);
		
		//Etiquetas y campo de entrada para modificar la posición en X del objeto.
		JLabel labelPosX = new JLabel("Posición en X:");
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelPosX, gbc);
		
		txtPosX = new JTextField(5);
		txtPosX.setText(""+objeto.position.x);
		gbc.gridx = 1;
		gbc.gridy = 3;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(txtPosX, gbc);
		
		//Etiquetas y campo de entrada para modificar la posición en Y del objeto.
		JLabel labelPosY = new JLabel("Posición en Y:");
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelPosY, gbc);
		
		txtPosY = new JTextField(5);
		txtPosY.setText(""+objeto.position.y);
		gbc.gridx = 1;
		gbc.gridy = 4;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(txtPosY, gbc);
		
		//Etiquetas y campo de entrada para modificar la velocidad del objeto.
		JLabel labelVelocidad = new JLabel("Velocidad:");
		gbc.gridx = 0;
		gbc.gridy = 5;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelVelocidad, gbc);
		
		txtVelocidad = new JTextField(5);
		txtVelocidad.setText(""+objeto.velocity);
		gbc.gridx = 1;
		gbc.gridy = 5;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(txtVelocidad, gbc);
		
		JLabel labelVelocidadUnidad = new JLabel("m/s²");
		gbc.gridx = 2;
		gbc.gridy = 5;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelVelocidadUnidad, gbc);
		
		//Botón para aceptar los cambios.
		botonAcceptar = new JButton("Acceptar");
		botonAcceptar.addActionListener(this);
		gbc.gridx = 0;
		gbc.gridy = 7;
		gbc.insets = new Insets(20, 10, 10, 10);
		myPanel.add(botonAcceptar, gbc);
		
		//Botón para no hacer ningún cambio (cancelar).
		botonCancelar = new JButton("Cancelar");
		botonCancelar.addActionListener(this);
		gbc.gridx = 1;
		gbc.gridy = 7;
		gbc.insets = new Insets(20, 10, 10, 10);
		myPanel.add(botonCancelar, gbc);
		
		this.add(myPanel);
		
	}
	
	public void SetSJ(SwingJuego a)
	{
		this.SJ=a;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent event)
	{
		if(event.getSource() == botonAcceptar)
		{
			if( verificarAll() )
			{
				int angulo = Integer.parseInt(txtAngulo.getText());
				int vel = Integer.parseInt(txtVelocidad.getText());
				int masa = Integer.parseInt(txtMasa.getText());
				int grav = Integer.parseInt(txtGravedad.getText());
				int X = Integer.parseInt(txtPosX.getText());
				int Y = Integer.parseInt(txtPosY.getText());
				SJ.UpdateCaract(angulo, vel, masa, grav, X, Y);
				this.dispose();
			}
			
			else 
			{
				String mensajeError = "<html><pre>No se han podido modificar las variables (problema con el formato ingresado)." + "\nDebe llenar todos los campos";
				SwingError SE= new SwingError(mensajeError);
				SE.setVisible(true);
			}		
		}
		
		else
		{
			this.dispose();
		}
	}
	

//adaptado a partir de: http://stackoverflow.com/questions/8391979/does-java-have-a-int-tryparse-that-doesnt-throw-an-exception-for-bad-data

/**
 * método que sirve para saber si el formato es adecuado (es entero)
 * @param txt, min, max
 * @return true si se puede convertir a un entero, false en caso contrario
 */
public boolean verificarFormato(String txt)
{
	try
	{
		Integer.parseInt(txt);
		return true;
	}
	
	catch (NumberFormatException nfe)
	{
		return false;
	}
	
}

	/**
	 * método que sirve para saber si el formato es adecuado (es entero, dentro del rango deseado)
	 * @param txt, min, max
	 * @return true si se puede convertir a un entero y si está dentro del rango deseado, false en caso contrario
	 */
	public boolean verificarFormato(String txt, int min, int max)
	{
		try
		{
			Integer.parseInt(txt);
			
			if(Integer.parseInt(txt)>=min && Integer.parseInt(txt)<=max)
			{
			return true;
			}
			
			else return false;
		}
		
		catch (NumberFormatException nfe)
		{
			return false;
		}
		
	}
	
	public boolean verificarAll()
	{
		if ( 	verificarFormato(txtAngulo.getText()) &&
				verificarFormato(txtGravedad.getText()) &&
				verificarFormato(txtVelocidad.getText()) &&
				verificarFormato(txtMasa.getText()) &&
				verificarFormato(txtPosX.getText()) &&
				verificarFormato(txtPosY.getText())
				)
		{
			return true;
		}
		else return false;
		
	}
	
	
	
	
}

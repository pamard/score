package scoreTest;

import static org.junit.Assert.*;

import java.awt.Point;
import java.net.MalformedURLException;

import org.junit.Test;

import pEventsUtil.pEvent;
import Backend.EntityFactory;
//import Backend.Juego;
//import Frontend.Juego;

public class JuegoTest {
	
	public Backend.Juego J;

	//Test que se encarga de verificar la correcta modelacion de el lanzamiento de proyectil	
	@Test
	public void test() 
	{
		J = Backend.Juego.Instance();
		Frontend.Juego.ClickEvent = new pEvent();
		Frontend.Juego.ClickEvent.addEventListener(J);
		Frontend.Juego FJ = null;
		try 
		{
			FJ = new Frontend.Juego(true,0);
		} 
		catch (MalformedURLException e) 
		{
			e.printStackTrace();
		}
		
		int angulo = 70;
		int grav = 10;
		int masa = 1;
		Point pos = new Point(20,0);
		int vel = 20;
		String nombre = "test";
		int tam = 1;
		FJ.ClickEvent.fireEvent(2, angulo, grav, masa, nombre, pos, tam, vel);
		J.Simular(0);
		Point p = J.lastPosition;
		assertTrue(p.x == 49 && p.y == 0);
				
	}

}
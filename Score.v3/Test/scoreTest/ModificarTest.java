package scoreTest;

import static org.junit.Assert.*;
import Frontend.*;
import Backend.*;
import Backend.Juego;
import Backend.EntityFactory.Ball;
import pEventsUtil.*;

import org.junit.Test;

import java.awt.*;
import java.net.MalformedURLException;

public class ModificarTest {
	
	Backend.Juego BJ;
	Modificar M;

	//test para verificar el metodo de formato de la ventana modificar.
	@Test
	public void FormatoCorrectoTest() 
	{
		BJ = Juego.Instance();
		Ball e=EntityFactory.Instance().createBall();
		EntityFactory.Instance().DatafromBall(e, 1, "prueba", 1);
		EntityFactory.Instance().DatafromLaunch(e, 1, 1, new Point(1,1), 1);
		Backend.Juego.objetos.add(e);
		M = new Modificar(0,0);
		boolean prueba = M.verificarFormato("45");
		assertTrue(prueba == true);
	}
	
	@Test
	public void FormatoIncorrectoTest()
	{
		BJ = Backend.Juego.Instance();
		Ball e=EntityFactory.Instance().createBall();
		EntityFactory.Instance().DatafromBall(e, 1, "prueba", 1);
		EntityFactory.Instance().DatafromLaunch(e, 1, 1, new Point(1,1), 1);
		Backend.Juego.objetos.add(e);
		M = new Modificar(0,0);
		boolean prueba = M.verificarFormato("hola");
		assertTrue(prueba == false);				
	}
	
	//El siguiente test verifica que cuando se realizan cambios en la ventana modificar
	//estos realmente cambien los campos del objeto en el backend
	@Test
	public void FuncionamientoTest()
	{
		Backend.Juego BJ;
		BJ = Backend.Juego.Instance();
		Ball b=EntityFactory.Instance().createBall();
		EntityFactory.Instance().DatafromBall(b, 0, "prueba", 0);
		EntityFactory.Instance().DatafromLaunch(b, 0, 0, new Point(0,0), 0);
		
		BJ.objetos.add(b);
		Frontend.Juego.ClickEvent = new pEvent();
		Frontend.Juego.ClickEvent.addEventListener(BJ);
		Frontend.Juego FJ = null;
		try {
			FJ = new Frontend.Juego(true, 0);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int angulo = 1;
		int grav = 1;
		int masa = 1;
		Point pos = new Point(1,1);
		int vel = 1;
		int posicionObjeto = 0;
		FJ.ClickEvent.fireEvent(3,posicionObjeto, angulo, grav, masa, pos, vel);
		
		assertTrue(BJ.objetos.get(0).angle == 1 &&
				BJ.objetos.get(0).gravity == 1 &&
				BJ.objetos.get(0).mass == 1 &&
				BJ.objetos.get(0).positionInicial.x==1 &&
				BJ.objetos.get(0).positionInicial.y==1 &&
				BJ.objetos.get(0).velocity == 1);
	}

}

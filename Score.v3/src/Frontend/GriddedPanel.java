package Frontend;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Stroke;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Line2D;
import java.util.*;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import Backend.EntityFactory;

//clase a partir de: https://answers.yahoo.com/question/index?qid=20081118204215AAigMyf
public class GriddedPanel extends JPanel {

	int width, height, rows, columns;
	public Color colorGrilla;
	public Juego frontJuego;
	Graphics g;

	public GriddedPanel(int w, int h, int r, int c, Juego juego) {

		width = w;
		height = h;
		rows = r;
		columns = c;
		setLayout(null);
		colorGrilla = Color.black;
		frontJuego = juego;
	}

	public void paintComponent(Graphics g) {

		setSize(width, height);
		int wide = getWidth();
		int tall = getHeight();
		int w = wide / columns;
		int h = tall / rows;
		this.g = g;
		
		// fuente:
		// http://examplecode.wordpress.com/2010/02/03/how-to-draw-a-dotted-or-dashed-line-in-java/
		Graphics2D g2 = (Graphics2D) g;
		Stroke dotted = new BasicStroke(1, BasicStroke.CAP_BUTT,
				BasicStroke.JOIN_BEVEL, 0, new float[] { 1, 2 }, 0);

		g2.setStroke(dotted);
		g2.setColor(colorGrilla);

		// Se dibujan las columnas
		for (int i = 1; i < columns; i++) {		
			g2.drawLine(i * w, 0, i * w, tall);
		}

		// Se dibujan las filas
		for (int i = 1; i < rows; i++) {
			g2.drawLine(0, i * h, wide, i * h);
		}


		double rowH = getHeight() / 10.0;
		for (int i = 1; i < rows; i++) {
			Line2D line = new Line2D.Double(0.0, (double) i * rowH,
					(double) getWidth(), (double) i * rowH);
			g2.draw(line);
		}

		double heightW = getWidth() / 10.0;
		for (int i = 1; i < columns; i++) {
			Line2D line = new Line2D.Double((double) i * heightW, 0.0,
					(double) i * heightW, (double) getHeight());
			g2.draw(line);
		}

		//se pintan las casillas en las que se encuentran todos los objetos creados
		for(int i =0; i<Backend.Juego.objetos.size(); i++)
		{
			if(frontJuego.comboBoxObjetos.getSelectedIndex() == i+1)
			{
				pintarCasilla(Backend.Juego.objetos.get(i).position, Backend.Juego.objetos.get(i).tamanio, 3);
			}
			
			else
			{
			pintarCasilla(Backend.Juego.objetos.get(i).position, Backend.Juego.objetos.get(i).tamanio, 0);
			}
		}
		
		if(Juego.posMetaSeleccionada != null)
		{
			pintarCasilla(Juego.posMetaSeleccionada,Juego.tamMetaSeleccionada,1);
		}
		
		//se pintan las casillas en las que se encuentran todos los obst�culos del nivel
		for (int i=0;i<Juego.obstaculosSelected.size();i++)
		{
			EntityFactory.Obstaculo ob = Juego.obstaculosSelected.get(i);
			pintarCasilla(ob.position,ob.tamanio,2);
		}
	}

	/**
	 * M�todo que permite mostrar de forma gr�fica d�nde se ubica el objeto
	 * 
	 * @param pos
	 *            = posici�n del objeto
	 * @param tamanio
	 *            = tama�o del objeto
	 */
	public void pintarCasilla(Point pos, int tamanio,int color) {
		if(pos.x > 49)
		{
			pos.x -= 49;
		}
		
		if(pos.x<0)
		{
			pos.x += 49;
		}
		
		if (color == 0 || color == 3){
			if(color == 0) g.setColor(Color.red);
			else g.setColor(Color.cyan);
			
			// se multiplican los valores por 16 y 12 para escalar los pixeles a los cuadrados de la grilla
			g.fillOval(pos.x * 16, (49-pos.y) * 12, tamanio * 16, tamanio * 12);
		}
		
		else
		{
			if (color == 1 )
		{
			g.setColor(Color.green);
		}
			
		else if (color == 2)
		{
			g.setColor(Color.darkGray);
		}
			
			// se multiplican los valores por 16 y 12 para escalar los pixeles a los cuadrados de la grilla
			g.fillRect(pos.x * 16, (49-pos.y) * 12, tamanio * 16, tamanio * 12);
		}


	}

}

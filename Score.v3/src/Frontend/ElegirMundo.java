package Frontend;




import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.net.MalformedURLException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
//Importo el packete de eventos
import pEventsUtil.*;
import Images.*;

//esta clase representa la ventana que permite elegir el mundo en el que se jugar� (modo competitivo).
public class ElegirMundo extends Gui{

	public JPanel myPanel;
	public GridBagConstraints gbc;
	private JButton mundoMercurio;
	private JButton mundoTierra;
	private JButton mundoJupiter;
	//Creo el evento
	public static pEvent mundoElegidoEvent;
	
	public ElegirMundo() throws MalformedURLException
	{
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		myPanel = new JPanel();
		myPanel.setLayout(new GridBagLayout());
		gbc = new GridBagConstraints();
		
		//Se crea y se ubica la etiqueta de instrucciones
		JLabel labelInstruccion = new JLabel("<html><pre>Haga click en el mundo en el que desea jugar:</pre></html>");
		gbc.gridy = 0;
		gbc.gridx = 1;
		gbc.insets = new Insets(10, 10, 10, 10);
		myPanel.add(labelInstruccion, gbc);
		
		
		//Se crean y se ubican la imagen, el bot�n y la etiqueta para el mundo Mercurio
		ImageIcon imagenMercurio = new ImageIcon(new java.net.URL(getClass().getResource("/Images/Mercurio.jpg")+""));
		Image img = imagenMercurio.getImage();  
		Image newimg = img.getScaledInstance(300, 400,  java.awt.Image.SCALE_SMOOTH);  
		imagenMercurio = new ImageIcon(newimg);
		
		mundoMercurio = new JButton(imagenMercurio);
		mundoMercurio.addActionListener(this);
		gbc.gridy = 1;
		gbc.gridx = 0;
		myPanel.add(mundoMercurio, gbc);
		
		JLabel labelMercurio = new JLabel("<html><pre>\tMercurio\n Gravedad: 3 m/s�</pre></html>");
		gbc.gridy = 2;
		gbc.gridx = 0;
		gbc.insets = new Insets(10, 10, 10, 10);
		myPanel.add(labelMercurio, gbc);
		
		
		//Se crean y se ubican la imagen, el bot�n y la etiqueta para el mundo Tierra
		ImageIcon imagenTierra = new ImageIcon (new java.net.URL(getClass().getResource("/Images/Tierra.jpg")+""));
		img = imagenTierra.getImage();  
		newimg = img.getScaledInstance(300, 400,  java.awt.Image.SCALE_SMOOTH);  
		imagenTierra = new ImageIcon(newimg);
		
		mundoTierra = new JButton(imagenTierra); 
		mundoTierra.addActionListener(this);
		gbc.gridy = 1;
		gbc.gridx = 1;
		myPanel.add(mundoTierra, gbc);
		
		JLabel labelTierra = new JLabel("<html><pre>\tTierra\n Gravedad: 10 m/s�</pre></html>");
		gbc.gridy = 2;
		gbc.gridx = 1;
		gbc.insets = new Insets(10, 10, 10, 10);
		myPanel.add(labelTierra, gbc);
		
		
		//Se crean y se ubican la imagen, el bot�n y la etiqueta para el mundo Jupiter
		ImageIcon imagenJupiter = new ImageIcon(new java.net.URL(getClass().getResource("/Images/Jupiter.jpg")+""));
		img = imagenJupiter.getImage();  
		newimg = img.getScaledInstance(300, 400,  java.awt.Image.SCALE_SMOOTH);  
		imagenJupiter = new ImageIcon(newimg);
		
		mundoJupiter = new JButton(imagenJupiter); 
		mundoJupiter.addActionListener(this);
		gbc.gridy = 1;
		gbc.gridx = 2;
		myPanel.add(mundoJupiter, gbc);
	
		JLabel labelJupiter = new JLabel("<html><pre>\tJupiter\n  Gravedad: 23 m/s�</pre></html>");
		gbc.gridy = 2;
		gbc.gridx = 2;
		gbc.insets = new Insets(10, 10, 10, 10);
		myPanel.add(labelJupiter, gbc);
		
		
		this.add(myPanel);
	}
	
	@Override
	public void actionPerformed(ActionEvent event){
		
		//Caso que apret� el bot�n mundo Mercurio: se abre la ventana correspondiente.
		if(event.getSource() == mundoMercurio)
		{
			//Se abre la ventana de juego con el mundo escogido (Mercurio)
			Juego modoCompetitivoMercurio;
			
			try {
				modoCompetitivoMercurio = new Juego(false, 1);
				modoCompetitivoMercurio.setTitle("SCORE - MODO COMPETITIVO (MERCURIO)");
				modoCompetitivoMercurio.setExtendedState(Frame.MAXIMIZED_BOTH);
				modoCompetitivoMercurio.centreWindow();
				modoCompetitivoMercurio.setVisible(true);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		
		//Caso que apret� el bot�n mundo Tierra: se abre la ventana correspondiente.
		if(event.getSource() == mundoTierra)
		{
			//Se abre la ventana de juego con el mundo escogido (Tierra)
			Juego modoCompetitivoTierra;
			try {
				modoCompetitivoTierra = new Juego(false, 2);
				modoCompetitivoTierra.setTitle("SCORE - MODO COMPETITIVO (TIERRA)");
				modoCompetitivoTierra.setExtendedState(Frame.MAXIMIZED_BOTH);  
				modoCompetitivoTierra.centreWindow();
				modoCompetitivoTierra.setVisible(true);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		
		//Caso que apret� el bot�n mundo Jupiter: se abre la ventana correspondiente.
		if(event.getSource() == mundoJupiter)
		{
			//Se abre la ventana de juego con el mundo escogido (Jupiter)
			Juego modoCompetitivoJupiter;
			try {
				modoCompetitivoJupiter = new Juego(false, 3);
				modoCompetitivoJupiter.setTitle("SCORE - MODO COMPETITIVO (JUPITER)");
				modoCompetitivoJupiter.setExtendedState(Frame.MAXIMIZED_BOTH);  
				modoCompetitivoJupiter.centreWindow();
				modoCompetitivoJupiter.setVisible(true);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		
		//en cualquier caso, se cierra esta ventana.
		this.dispose();
	}
	
	
	
	
}
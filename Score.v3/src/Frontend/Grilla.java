package Frontend;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JPanel;
import java.awt.Point;


public class Grilla {
	
	private JPanel grilla;
	final private int cols=100;
	final private int rows =100;
	
	
	public Grilla()
	{
	
	}
	
	public JPanel getGrilla(Point pos, int[][] objetos)//se le entrega la pocicion de la pelota
	{
		grilla= new JPanel();
		grilla.setBackground(new Color(0,0,0,0));
		final GridLayout gArray = new GridLayout(rows, cols, 0 , 0 );//Filas x Columnas
		grilla.setLayout(gArray);
		
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				if(objetos[i][j]==1)
				{
					grilla.add(getGridElement(1, new Point(i,j)));
					
				}
				else if( i==rows/2 && j==cols/2)
				{
					grilla.add(getGridElement(2, new Point(i,j)));
				}
					
				else
				{
					grilla.add(getGridElement(0, new Point(i,j)));
				}
				

			}
		}
		
		return grilla;
		
		
	}
	private JPanel getGridElement(int element, Point pos)
	{
		
		@SuppressWarnings("serial")
		class gridElement extends JPanel
		{
			private final Dimension d = new Dimension(400/cols , 300/rows);//Ancho x Alto
			public gridElement(int element)
			{
				setMinimumSize(d);
				setMaximumSize(d);
				setPreferredSize(d);
				
				if(element==1)
				{
					setBackground(Color.BLACK);
					
				}
				
				else if(element==2)
				{
					setBackground(Color.RED);
				}
				else
				{
					setBackground(new Color(0,0,0,0));
				}
			}
			
		}
		
		
		return new gridElement(element);
	}
	

}
package Frontend;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

//esta clase representa la ventana que permite modificar características del objeto.
public class Modificar extends Gui {

	public int mundo;
	public int posicionObjeto;
	public JPanel myPanel;
	public JButton botonAcceptar;
	public JButton botonCancelar;
	private JTextField txtAngulo;
	JLabel labelValorGravedad;
	private JTextField txtGravedad;
	private JTextField txtMasa;
	private JTextField txtPosX;
	private JTextField txtPosY;
	private JTextField txtVelocidad;
	
	
	public Modificar(int mundo, int posicionObjeto)
	{
		setResizable( false );
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		this.mundo = mundo;
		this.posicionObjeto = posicionObjeto;
		
		myPanel = new JPanel();
		myPanel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		//Etiquetas del nombre del objeto.
		JLabel labelNombre = new JLabel("Nombre:");
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelNombre, gbc);
		
		JLabel nombre = new JLabel(Backend.Juego.objetos.get(posicionObjeto).name);
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(nombre, gbc);
		
		//Etiquetas y campo de entrada para modificar el �ngulo del objeto.
		JLabel labelAngulo = new JLabel("�ngulo:");
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelAngulo, gbc);
		
		txtAngulo = new JTextField(5);
		txtAngulo.setText(""+Backend.Juego.objetos.get(posicionObjeto).angle);
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(txtAngulo, gbc);
		
		JLabel labelAnguloUnidad = new JLabel("grados");
		gbc.gridx = 2;
		gbc.gridy = 1;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelAnguloUnidad, gbc);
		
		//Etiquetas y campo de entrada para modificar la gravedad del objeto.
		JLabel labelGravedad = new JLabel("Gravedad:");
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelGravedad, gbc);
		
		//solo se puede modificar en el modo de prueba (mundo = 0)
		if(mundo == 0){
		txtGravedad = new JTextField(5);
		txtGravedad.setText(""+Backend.Juego.objetos.get(posicionObjeto).gravity);
		gbc.gridx = 1;
		gbc.gridy = 2;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(txtGravedad, gbc);
		}
		
		else
		{	
			switch(mundo){
				case 1:
					labelValorGravedad = new JLabel("3");
					break;
				case 2:
					labelValorGravedad = new JLabel("10");
					break;
				default: 
					labelValorGravedad = new JLabel("23");
					break;
					}

			gbc.gridx = 1;
			gbc.gridy = 2;
			gbc.insets = new Insets(10, 0, 0, 10);
			myPanel.add(labelValorGravedad, gbc);
		}
		
		JLabel labelGravedadUnidad = new JLabel("m/s�");
		gbc.gridx = 2;
		gbc.gridy = 2;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelGravedadUnidad, gbc);
		
		//Etiquetas y campo de entrada para modificar la masa del objeto.
		JLabel labelMasa = new JLabel("Masa:");
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelMasa, gbc);
		
		txtMasa = new JTextField(5);
		txtMasa.setText(""+Backend.Juego.objetos.get(posicionObjeto).mass);
		gbc.gridx = 1;
		gbc.gridy = 3;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(txtMasa, gbc);
		
		JLabel labelMasaUnidad = new JLabel("kg");
		gbc.gridx = 2;
		gbc.gridy = 3;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelMasaUnidad, gbc);
		
		//Etiquetas y campo de entrada para modificar la posici�n en X del objeto.
		JLabel labelPosX = new JLabel("Posici�n en X:");
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelPosX, gbc);
		
		txtPosX = new JTextField(5);
		txtPosX.setText(""+Backend.Juego.objetos.get(posicionObjeto).position.x);
		gbc.gridx = 1;
		gbc.gridy = 4;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(txtPosX, gbc);
		
		//Etiquetas y campo de entrada para modificar la posici�n en Y del objeto.
		JLabel labelPosY = new JLabel("Posici�n en Y:");
		gbc.gridx = 0;
		gbc.gridy = 5;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelPosY, gbc);
		
		txtPosY = new JTextField(5);
		txtPosY.setText(""+Backend.Juego.objetos.get(posicionObjeto).position.y);
		gbc.gridx = 1;
		gbc.gridy = 5;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(txtPosY, gbc);
		
		//Etiquetas y campo de entrada para modificar la velocidad del objeto.
		JLabel labelVelocidad = new JLabel("Velocidad:");
		gbc.gridx = 0;
		gbc.gridy = 6;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelVelocidad, gbc);
		
		txtVelocidad = new JTextField(5);
		txtVelocidad.setText(""+Backend.Juego.objetos.get(posicionObjeto).velocity);
		gbc.gridx = 1;
		gbc.gridy = 6;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(txtVelocidad, gbc);
		
		JLabel labelVelocidadUnidad = new JLabel("m/s�");
		gbc.gridx = 2;
		gbc.gridy = 6;
		gbc.insets = new Insets(10, 0, 0, 10);
		myPanel.add(labelVelocidadUnidad, gbc);
		
		//Bot�n para aceptar los cambios.
		botonAcceptar = new JButton("Aceptar");
		botonAcceptar.addActionListener(this);
		gbc.gridx = 0;
		gbc.gridy = 8;
		gbc.insets = new Insets(20, 10, 10, 10);
		myPanel.add(botonAcceptar, gbc);
		
		//Bot�n para no hacer ningún cambio (cancelar).
		botonCancelar = new JButton("Cancelar");
		botonCancelar.addActionListener(this);
		gbc.gridx = 1;
		gbc.gridy = 8;
		gbc.insets = new Insets(20, 10, 10, 10);
		myPanel.add(botonCancelar, gbc);
		
		this.add(myPanel);
		
	}
	
	
	@Override
	public void actionPerformed(ActionEvent event)
	{
		if(event.getSource() == botonAcceptar)
		{
			if( verificarAll() )
			{	
				int angulo = Integer.parseInt(txtAngulo.getText());
				int grav;
				if(mundo == 0)
				{
				grav = Integer.parseInt(txtGravedad.getText());
				}
				else
				{
					grav = Integer.parseInt(labelValorGravedad.getText());
				}
				int masa = Integer.parseInt(txtMasa.getText());
				int X = Integer.parseInt(txtPosX.getText());
				int Y = Integer.parseInt(txtPosY.getText());
				Point pos = new Point(X, Y);
				int vel = Integer.parseInt(txtVelocidad.getText());

				Juego.ClickEvent.fireEvent(3, posicionObjeto, angulo, grav, masa, pos, vel);
				this.dispose();
			}
			
			else 
			{
				String mensajeError = "<html><pre>No se han podido modificar las variables (problema con el formato ingresado)." + "\nDebe llenar todos los campos";
				JOptionPane.showMessageDialog(null,mensajeError,"Parsing Error",JOptionPane.ERROR_MESSAGE);
				
			}		
		}
		
		else
		{
			this.dispose();
		}
	}
	

	//adaptado a partir de: http://stackoverflow.com/questions/8391979/does-java-have-a-int-tryparse-that-doesnt-throw-an-exception-for-bad-data

	/**
	 * m�todo que sirve para saber si el formato es adecuado (es entero)
	 * @param txt, min, max
	 * @return true si se puede convertir a un entero, false en caso contrario
	 */
		
	public boolean verificarFormato(String txt)
	{
		try
		{
			Integer.parseInt(txt);
			return true;
		}
		
		catch (NumberFormatException nfe)
		{
			return false;
		}
		
	}

		/**
		 * m�todo que sirve para saber si el formato es adecuado (es entero, dentro del rango deseado)
		 * @param txt, min, max
		 * @return true si se puede convertir a un entero y si está dentro del rango deseado, false en caso contrario
		 */
		public boolean verificarFormato(String txt, int min, int max)
		{
			try
			{
				Integer.parseInt(txt);
				
				if(Integer.parseInt(txt)>=min && Integer.parseInt(txt)<=max)
				{
				return true;
				}
				
				else return false;
			}
			
			catch (NumberFormatException nfe)
			{
				return false;
			}
			
		}
		
		/**
		 * m�todo que sirve para saber si el formato es adecuado (es entero, respetando un m�nimo establecido)
		 * @param txt, min, max
		 * @return true si se puede convertir a un entero y si est� dentro del rango deseado, false en caso contrario.
		 */
		public boolean verificarFormato(String txt, int min)
		{
			try
			{
				Integer.parseInt(txt);
				
				if(Integer.parseInt(txt)>=min)
				{
				return true;
				}
				
				else return false;
			}
			
			catch (NumberFormatException nfe)
			{
				return false;
			}
			
		}
		
		/**
		 * m�todo para verificar que todos los par�metros del objeto cumplan con las condiciones de borde
		 * @return true si se cumplen todos los requisitos, false en caso contrario.
		 */
		public boolean verificarAll()
		{
			if ( 	//el �ngulo debe cumplir con un rango (-90, 90)
					verificarFormato(txtAngulo.getText(), -90, 90) &&
					
					//la gravedad debe cumplir con un m�nimo (0) y solo se verifica el formato en el modo de prueba
					((mundo != 0) || verificarFormato(txtGravedad.getText(), 0)) &&
					
					//la velocidad debe cumplir con un m�nimo (0)
					verificarFormato(txtVelocidad.getText(), 0) &&
					
					//la masa debe cumplir con un m�nimo
					verificarFormato(txtMasa.getText(), 0) &&
					
					//no hay l�mite respecto de la posici�n (dado que se considera un mundo infinito)
					verificarFormato(txtPosX.getText()) &&
					verificarFormato(txtPosY.getText())
					)
			{
				return true;
			}
			
			else return false;
			
		}
}
package Frontend;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;

import pEventsUtil.pEvent;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;


//esta clase representa la ventana del juego. En ella, se pueden cambiar las caracter�sticas del objeto y lanzar.
public class Juego extends Gui {

	//objetos que componen la interfaz gr�fica
	public JPanel myPanel;
	public static JButton BotonCrear;
	public static JButton BotonModificar;
	public static JButton BotonLanzar;
	public JButton BotonVolver;
	public JComboBox comboBoxObjetos;
	public JLabel caracteristicasObjeto;
	public JLabel pastoCancha;
	public JLabel lvl;
	public GridBagConstraints gbc;
	
	public GriddedPanel vistaGrilla;
	public int nvl;
	public boolean modoPrueba;
	public int mundo;
	public static String informeCaracteristicasObjeto;
	public static Point posMetaSeleccionada;
	public static int tamMetaSeleccionada;
	public static List<Backend.EntityFactory.Obstaculo> obstaculosSelected;

	// Se crea el evento
	public static pEvent ClickEvent;

	public Juego(boolean modo, int numMundo) throws MalformedURLException {
		ClickEvent.fireEvent(0, this);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		informeCaracteristicasObjeto = "Debe crear alguna pelota.";

		modoPrueba = modo;
		mundo = numMundo;
		myPanel = new JPanel();
		myPanel.setLayout(new GridBagLayout());
		gbc = new GridBagConstraints();

		//se crea un panel auxiliar para superponer la imagen de fondo con la grilla
		JPanel aux = new JPanel();
		aux.setLayout(new OverlayLayout(aux));
		
		// Se configura la grilla de vista y se agrega al panel auxiliar para poder simular gr�ficamente el movimiento
		vistaGrilla =  new GriddedPanel(800, 600, 50, 50, this);
		aux.add(vistaGrilla);
		
		// Se crea y se ubica un fondo para el desplazamiento de la pelota
		pastoCancha = SetPastoCancha();
		aux.add(pastoCancha);
		
		//se agrega el panel auxiliar al panel principal
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 10;
		gbc.gridheight = 5;
		gbc.insets = new Insets(100, 0, 10, 75);
		myPanel.add(aux, gbc);


		// Se crea y se ubica el bot�n que permite volver al men� principal.
		BotonVolver = new JButton("Volver al men� principal");
		BotonVolver.addActionListener(this);
		gbc.gridx = 10;
		gbc.gridy = 1;
		gbc.gridheight = 1;
		gbc.insets = new Insets(100, 75, 20, 75);
		myPanel.add(BotonVolver, gbc);
		
		//se crea y se ubica el label que muestra en qu� nivel est� el usuario
		nvl = 1;
		lvl = new JLabel("Nivel " + nvl);
		lvl.setForeground(Color.blue);
		lvl.setFont (lvl.getFont ().deriveFont (32.0f));
		gbc.gridx = 10;
		gbc.gridy = 2;
		gbc.gridheight = 1;
		gbc.insets = new Insets(20, 75, 40, 75);
		myPanel.add(lvl, gbc);
		
		// Se crea y se ubica la combo box que permitir� escoger la pelota
		String[] listaNombrePelotas = new String[1];
		listaNombrePelotas[0] = " Seleccione alguna pelota";
		SortedComboBoxModel model = new SortedComboBoxModel(listaNombrePelotas);
		comboBoxObjetos = new JComboBox(model);
		gbc.gridx = 10;
		gbc.gridy = 3;
		gbc.gridheight = 1;
		gbc.insets = new Insets(40, 75, 0, 75);
		myPanel.add(comboBoxObjetos, gbc);
		

		// se crea un action listener para detectar cuando se cambie la selecci�n y poder disparar un evento.
		comboBoxObjetos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(comboBoxObjetos.getSelectedIndex() != 0)
				{
				ClickEvent.fireEvent(4, comboBoxObjetos.getSelectedIndex()-1);
				UpdateCaract();
				}
				
				else
				{
					if(comboBoxObjetos.getItemCount() > 1)
					{
						informeCaracteristicasObjeto = "Debe seleccionar alguna pelota.";
					    UpdateCaract();
					}
					
					else
					{
						informeCaracteristicasObjeto = "Debe crear alguna pelota.";
					    UpdateCaract();	
					}
					
				}
			}
		});

		// se crea y se ubica la secci�n en la que se muestran las caracter�sticas del objeto.
		caracteristicasObjeto = new JLabel(informeCaracteristicasObjeto);
		gbc.gridx = 10;
		gbc.gridy = 3;
		gbc.gridheight = 5;
		gbc.insets = new Insets(0, 75, 0, 75);
		myPanel.add(caracteristicasObjeto, gbc);

		// Se crea y se ubica el bot�n que permite crear un nuevo objeto.
		BotonCrear = new JButton("Crear");
		BotonCrear.addActionListener(this);
		gbc.gridx = 2;
		gbc.gridy = 7;
		gbc.gridwidth = 2;
		gbc.insets = new Insets(0, 100, 100, 100);
		gbc.anchor = GridBagConstraints.BASELINE_LEADING;
		myPanel.add(BotonCrear, gbc);

		// Se crea y se ubica el bot�n que permite lanzar el objeto.
		BotonLanzar = new JButton("Lanzar");
		BotonLanzar.setEnabled(false);
		BotonLanzar.addActionListener(this);
		gbc.gridx = 4;
		gbc.gridy = 7;
		gbc.insets = new Insets(0, 100, 100, 100);
		gbc.anchor = GridBagConstraints.BASELINE_TRAILING;
		myPanel.add(BotonLanzar, gbc);

		// Se crea y se ubica el bot�n que permite modificar las caracter�sticas del objeto.
		BotonModificar = new JButton("Modificar");
		BotonModificar.setEnabled(false);
		BotonModificar.addActionListener(this);
		gbc.gridx = 6;
		gbc.gridy = 7;
		gbc.insets = new Insets(0, 100, 100, 100);
		gbc.anchor = GridBagConstraints.BASELINE_TRAILING;
		myPanel.add(BotonModificar, gbc);

		this.add(myPanel);

	}
	
	public void LevelUp(){
		nvl++;
		lvl.setText("Nivel " + nvl);
	}
	
	//m�todo que abre una ventana informando al usuario que le achunt� a la meta.
	public void showScore(String nombreObjeto)
	{	
		JOptionPane.showMessageDialog(null,"            Score! \n"+nombreObjeto+" le achunt� a la meta.","Score!",JOptionPane.INFORMATION_MESSAGE);
	}
	
	//m�todo que abre una ventana informando al usuario que choc� contra un obst�culo.
	public void showFail(String nombreObjeto)
	{
		JOptionPane.showMessageDialog(null,"            Game Over \n"+nombreObjeto+" choc� contra un obst�culo.","Game Over",JOptionPane.INFORMATION_MESSAGE);
	}

	//m�todo que abre una ventana informando al usuario que su objeto ya no tiene velocidad.
	public void showFail2(String nombreObjeto)
	{
		JOptionPane.showMessageDialog(null,"            Game Over \n"+nombreObjeto+" ya no tiene velocidad.","Game Over",JOptionPane.INFORMATION_MESSAGE);
	}
	
	public JLabel SetPastoCancha() throws MalformedURLException {
		ImageIcon imagenSuperficie;

		if (modoPrueba) {
			imagenSuperficie = new ImageIcon(new java.net.URL(getClass().getResource("/Images/sky2.jpg")+""));
		}

		// en caso de ser modo competitivo y seg�n el mundo escogido.
		else {

			// mundo Mercurio, imagen de superficie espec�fica.
			if (mundo == 1) {
				imagenSuperficie = new ImageIcon(new java.net.URL(getClass().getResource("/Images/SuperficieMercurio2.jpg")+""));
			}

			// mundo Tierra, imagen de superficie espec�fica.
			else if (mundo == 2) {
				imagenSuperficie = new ImageIcon(new java.net.URL(getClass().getResource("/Images/SuperficieTierra.jpg")+""));
			}

			// mundo Jupiter, imagen de superficie espec�fica.
			else {
				imagenSuperficie = new ImageIcon(new java.net.URL(getClass().getResource("/Images/SuperficieJupiter.jpg")+""));
			}

		}

		Image img = imagenSuperficie.getImage();
		Image newimg = img.getScaledInstance(800, 600,
				java.awt.Image.SCALE_SMOOTH);
		return new JLabel(new ImageIcon(newimg));
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		
		// en caso de apretar el bot�n modificar, se abre la ventana correspondiente.
		if (event.getSource() == BotonModificar) {
			// Setiando todos los componentes base de la venta Modificar
			Modificar SM = new Modificar(mundo, comboBoxObjetos.getSelectedIndex()-1);
			SM.setTitle("MODIFICAR OBJETO");
			SM.setSize(300, 350);
			SM.setVisible(true);
			SM.centreWindow();
		}

		// en caso de apretar el bot�n lanzar, se simula el movimiento del objeto en el backend.
		else if (event.getSource() == BotonLanzar) {
			// disparo el evento
			ClickEvent.fireEvent(1);
		}

		// en caso de apretar el bot�n crear, se permite crear un nuevo objeto.
		else if (event.getSource() == BotonCrear) {
			// Setiando todos los componentes base de la venta Crear
			Crear SC = new Crear(mundo);
			SC.setTitle("CREAR OBJETO");
			SC.setSize(300, 350);
			SC.centreWindow();
			SC.setVisible(true);
		}

		// en caso de apretar el bot�n volver al men� principal, se cierra esta ventana y se abre una del men� principal (SwingElegirModo).
		else {
			ElegirModo swing;
			
			try {
				swing = new ElegirModo();
				swing.setTitle("BIENVENIDOS");
				swing.setSize(500, 500);
				swing.setVisible(true);
				swing.centreWindow();
				
			} 
			
			catch (MalformedURLException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			//se eliminan los objetos creados durante el juego
			Backend.Juego.objetos = new ArrayList<Backend.EntityFactory.Ball>();
			this.dispose();
		}

	}


	public void UpdateCaract() {
		caracteristicasObjeto.setText(informeCaracteristicasObjeto);
		repaint();
	}

	public void UpdateListaPelotas(String item) {
		comboBoxObjetos.addItem(item);
		comboBoxObjetos.setSelectedItem(item);
	}

}

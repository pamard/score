package Frontend;

import java.util.Arrays;

import javax.swing.DefaultComboBoxModel;

//Clase a partir de: http://stackoverflow.com/questions/7387299/dynamically-adding-items-to-a-jcombobox
public class SortedComboBoxModel extends DefaultComboBoxModel {

        public SortedComboBoxModel() {
            super();
        }

        public SortedComboBoxModel(String[] lista) {
            Arrays.sort(lista);
            int size = lista.length;
            for (int i = 0; i < size; i++) {
                super.addElement(lista[i]);
            }
        }

        @Override
        public void addElement(Object element) {
            insertElementAt(element, 0);
        }

        @Override
        public void insertElementAt(Object element, int index) {
            int size = getSize();
            //  Determine where to insert element to keep model in sorted order            
            for (index = 0; index < size; index++) {
                Comparable c = (Comparable) getElementAt(index);
                if (c.compareTo(element) > 0) {
                    break;
                }
            }
            super.insertElementAt(element, index);
        }
    }

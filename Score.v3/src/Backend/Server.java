package Backend;

import java.net.*;
import java.io.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
 
/**
 * 
 * @author Base de Grupo 8
 *
 */
public class Server implements Runnable {
	public boolean corriendo;
    public void listen() throws IOException, SAXException, ParserConfigurationException 
    {
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(2222);
        } catch (IOException e) {
            serverSocket = new ServerSocket(0);
            System.err.println("Could not listen on port: 1234. Now listening in "+serverSocket.getLocalPort());
        }
		while(corriendo)
		{
	        Socket clientSocket = null;
	        try {
	            clientSocket = serverSocket.accept();
	            System.out.println("Conexion establecida");
	        } catch (IOException e) {
	            System.err.println("Accept failed.");
	            System.exit(1);
	        }
	        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
	        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			String xml = in.readLine();
			System.out.println(xml);
			//DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			//InputSource is = new InputSource();
		    //is.setCharacterStream(new StringReader(xml));
		   // 
		    //Document doc = db.parse(is);
		}
		serverSocket.close();
	}
	@Override
	public void run() {
		try {
			listen();
		} catch (IOException | SAXException | ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
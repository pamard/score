package Backend;
import java.awt.Point;
import java.util.List;
import java.util.ArrayList;



public class Nivel {

	private int numObstac;
	private EntityFactory.Goal goal;
	private List<EntityFactory.Obstaculo> obstaculos;
	
	public Nivel()
	{
		obstaculos = new ArrayList<EntityFactory.Obstaculo>();
		
		//if (level==0){
			goal = EntityFactory.Instance().createGoal(new Point(12, 17), 4);
			numObstac = 2;
			EntityFactory.Obstaculo ob1 = EntityFactory.Instance().createObstaculo(new Point(12,25), 2);
			EntityFactory.Obstaculo ob2 = EntityFactory.Instance().createObstaculo(new Point(15,5), 3);
			obstaculos.add(ob1);
			obstaculos.add(ob2);
		//}
	}
	
	public List<EntityFactory.Obstaculo> getObstaculos()
	{
		return obstaculos;
	}

	public int getNumObstac() {
		return numObstac;
	}

	public void setNumObstac(int numObstac) {
		this.numObstac = numObstac;
	}

	public EntityFactory.Goal getMeta() {
		return goal;
	}
	
	public void SetNivel(int level){
		if (level==1) {
			obstaculos.clear();
			goal = EntityFactory.Instance().createGoal(new Point(30, 30), 4);
			numObstac = 2;
			EntityFactory.Obstaculo ob1 = EntityFactory.Instance().createObstaculo(new Point(20,25), 2);
			EntityFactory.Obstaculo ob2 = EntityFactory.Instance().createObstaculo(new Point(12,7), 3);
			obstaculos.add(ob1);
			obstaculos.add(ob2);
		}
		else if (level == 2){
			obstaculos.clear();
			goal = EntityFactory.Instance().createGoal(new Point(30, 5), 4);
			numObstac = 3;
			EntityFactory.Obstaculo ob1 = EntityFactory.Instance().createObstaculo(new Point(10,10), 2);
			EntityFactory.Obstaculo ob2 = EntityFactory.Instance().createObstaculo(new Point(30,20), 3);
			EntityFactory.Obstaculo ob3 = EntityFactory.Instance().createObstaculo(new Point(20,30), 3);
			obstaculos.add(ob1);
			obstaculos.add(ob2);
			obstaculos.add(ob3);
		}
		else if (level == 3){
			obstaculos.clear();
			goal = EntityFactory.Instance().createGoal(new Point(45, 40), 3);
			numObstac = 3;
			EntityFactory.Obstaculo ob1 = EntityFactory.Instance().createObstaculo(new Point(20,10), 2);
			EntityFactory.Obstaculo ob2 = EntityFactory.Instance().createObstaculo(new Point(30,45), 3);
			EntityFactory.Obstaculo ob3 = EntityFactory.Instance().createObstaculo(new Point(10,20), 3);
			obstaculos.add(ob1);
			obstaculos.add(ob2);
			obstaculos.add(ob3);
		}
		else if (level == 4){
			obstaculos.clear();
			goal = EntityFactory.Instance().createGoal(new Point(20, 45), 4);
			numObstac = 3;
			EntityFactory.Obstaculo ob1 = EntityFactory.Instance().createObstaculo(new Point(30,20), 2);
			EntityFactory.Obstaculo ob2 = EntityFactory.Instance().createObstaculo(new Point(10,40), 3);
			EntityFactory.Obstaculo ob3 = EntityFactory.Instance().createObstaculo(new Point(5,20), 3);
			obstaculos.add(ob1);
			obstaculos.add(ob2);
			obstaculos.add(ob3);
		}
		else if (level == 5){
			obstaculos.clear();
			goal = EntityFactory.Instance().createGoal(new Point(45, 10), 3);
			numObstac = 4;
			EntityFactory.Obstaculo ob1 = EntityFactory.Instance().createObstaculo(new Point(30,30), 2);
			EntityFactory.Obstaculo ob2 = EntityFactory.Instance().createObstaculo(new Point(40,20), 3);
			EntityFactory.Obstaculo ob3 = EntityFactory.Instance().createObstaculo(new Point(30,40), 3);
			EntityFactory.Obstaculo ob4 = EntityFactory.Instance().createObstaculo(new Point(20,30), 3);
			obstaculos.add(ob1);
			obstaculos.add(ob2);
			obstaculos.add(ob3);
			obstaculos.add(ob4);
		}
	}
	
}

package Backend;


import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import Backend.EntityFactory.Ball;
/**
 * 
 * @author Pietro Rivera
 *
 */
public class XmlManager {
	
	public  void readXml(String input) throws ParserConfigurationException, SAXException, IOException
	{
		System.out.println(input);
		
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource is = new InputSource();
		    is.setCharacterStream(new StringReader(input));
		    
		
		    Document doc = db.parse(is);
		    NodeList nodes = doc.getElementsByTagName("Common");
		    
		    
		    
		    Element commons = (Element)nodes.item(0);
		    
		    NodeList node = commons.getElementsByTagName("PosX");
		    Element line = (Element) node.item(0);
		    int posX = Integer.parseInt(line.getTextContent());
		    
		    node = commons.getElementsByTagName("PosY");
		    line = (Element) node.item(0);
		    int posY = Integer.parseInt(line.getTextContent());
		    
		    node = commons.getElementsByTagName("Width");
		    line = (Element) node.item(0);
		    int width = Integer.parseInt(line.getTextContent());
		    
		    node = commons.getElementsByTagName("Height");
		    line = (Element) node.item(0);
		    int height = Integer.parseInt(line.getTextContent());
		    
		    node = commons.getElementsByTagName("OriginalGroupID");
		    line = (Element) node.item(0);
		    int originalId = Integer.parseInt(line.getTextContent());
		    
		    Point position=new Point(posX , posY);
		    
		    Random ran = new Random();
		    int angle = ran.nextInt(90);
		    int gravity = ran.nextInt(100);
		    int mass = ran.nextInt(50);
		    int velocity = ran.nextInt(100);
		    
		  Ball ball=EntityFactory.Instance().createBall();
		  EntityFactory.Instance().DatafromBall(ball, mass, "ID-"+originalId, height*width);
		  EntityFactory.Instance().DatafromLaunch(ball, angle, gravity, position, velocity);
		    
		   Juego.Instance().crearPelota(ball );
		    
		 //objetos.add(EntityFactory.Instance().createBall(angle, gravity, mass,  name,  position,  tamanio, velocity));
			

		     
		    
		
	    
	}
	
	public String writeXml(int posX , int posY , int width , int height , int original) throws ParserConfigurationException
	{
		
		
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			
			Document doc = docBuilder.newDocument();
			
			Element rootElement = doc.createElement("GameOfLife");
			doc.appendChild(rootElement);
			
			//Creo Nodo Common
			Element common = doc.createElement("Common");
			rootElement.appendChild(common);
			
			// Anado los atributos de common
			Element posXN = doc.createElement("PosX");
			posXN.appendChild(doc.createTextNode(posX+""));
			common.appendChild(posXN);
			
			Element posYN = doc.createElement("PosY");
			posYN.appendChild(doc.createTextNode(posY+""));
			common.appendChild(posYN);
			
			Element widthN = doc.createElement("Widt");
			widthN.appendChild(doc.createTextNode(width+""));
			common.appendChild(widthN);
			
			Element heightN = doc.createElement("Height");
			heightN.appendChild(doc.createTextNode(height+""));
			common.appendChild(heightN);
			
			Element originalN = doc.createElement("OriginalGroupID");
			originalN.appendChild(doc.createTextNode(original+""));
			common.appendChild(originalN);
			
			//Hay q hacer lo mismo para Grupo en particular
			
			
			//Escribir como xml
			
			DOMSource source = new DOMSource(doc);
			
			//Aplicacion de Transformacion en este caso a string, puede tb guardarse a archivo
			 
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer;
			
			StringWriter sw = new StringWriter();
			try {
				
				
			transformer = tf.newTransformer();
			
			
			// Si se quiere eliminar el encabezado del xml descomentar la sgte linea
			//transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			
	 
			transformer.transform(source, new StreamResult(sw));
			
			
			} catch (  TransformerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		
	 
			
			// En sw queda el string, a consola para pruebas 
			System.out.println(sw);
			return sw.toString();
			
			
			
			
			
		
 
	}
	

}

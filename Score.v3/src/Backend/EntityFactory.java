package Backend;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.EventObject;
import java.util.List;

import pEventsUtil.pEventListener;

public class EntityFactory 
{
	public Point position;
	public int tamanio;
	public BallComparator ballComparator;
	public List<Point> puntos;
	
	private static EntityFactory instance;
	
	private EntityFactory()
	{
		ballComparator= new BallComparator();
		this.puntos = new ArrayList<Point>();
	}
	
	public static EntityFactory Instance()
	{
		if(instance==null)
		{
			instance=new EntityFactory();
		}
		
		return instance;
	}
	
	public class BallComparator implements Comparator<Ball>
	{

		@Override
		public int compare(Ball b1, Ball b2) {
			// TODO Auto-generated method stub
			return b1.name.compareToIgnoreCase(b2.name);
		}
		
	}
	
	public class Ball extends EntityFactory
	{
		public int angle;
		public int gravity;
		public String name;
		public int mass;
		public Point positionInicial;
		public int velocity;
		public double Vx;
		public double Vy;
		public int initialVelocity;
		
		public void fromBall(int mass, String name, int tamanio)
		{
			this.mass=mass;
			this.name = name;
			this.tamanio=tamanio;
		}
		public void fromLaunch(int angle , int gravity , Point position ,int velocity)
		{
			this.angle=angle;
			this.gravity = gravity;
			this.position=position;
			this.positionInicial = position;
			this.velocity = velocity;
			this.initialVelocity = velocity;
			setVelocityComponents();
		}
		public Ball( )
		{
			this.puntos = new ArrayList<Point>();
		}
		
		public void setVelocityComponents()
		{
			this.Vx= (velocity*Math.cos(Math.toRadians(angle)));
			this.Vy= (velocity*Math.sin(Math.toRadians(angle)));			
		}
		
		public void getVelocity()
		{
			this.velocity = (int)(Math.sqrt(Math.pow(Vx, 2)+Math.pow(Vy, 2)));
		}
	
		
		/**
		 * m�todo que permite crear un informe con las caracter�sticas del objeto al momento de llamarlo.
		 * @return un string con el informe deseado
		 */
		public String informeCaracteristicas()
		{
			getVelocity();
			return "<html><pre>Caracter�sticas:"+
		"\nNombre: "+ name+
		"\n�ngulo: "+angle+" grados"+
		"\nGravedad: "+gravity+" m/s�"+
		"\nMasa: "+mass+" kg"+
		"\nPosici�n: ("+((int)position.getX()+", "+(int)position.getY())+ ")"+
		"\nTama�o: "+tamanio+" m"+
		"\nVelocidad: "+velocity+" m/s�</pre></html>";
			
		}
		
	}
	
	
	//Clase que permite crear obst�culos
	public class Obstaculo extends EntityFactory
	{
		public Obstaculo(Point position , int tamanio)
		{
			this.position=position;
			this.tamanio=tamanio;
			this.puntos = new ArrayList<Point>();
			setPuntos();
		}
		
		private void setPuntos()
		{
			Point aux = this.position;
			for (int i = 0; i< tamanio;i++)
			{
				aux = new Point(position.x,position.y-i);
				for (int j = 0; j < tamanio; j++)
				{
					this.puntos.add(aux);
					aux = new Point(aux.x+1,aux.y);
				}
			}			
		}
	}
	
	
	//Clase que permite crear la meta
	public class Goal extends EntityFactory
	{
		public Goal(Point position , int tamanio)
		{
			this.position=position;
			this.tamanio=tamanio;
			this.puntos = new ArrayList<Point>();
			setPuntos();
		}
		
		private void setPuntos()
		{
			Point aux = this.position;
			for (int i = 0; i< tamanio;i++)
			{
				aux = new Point(position.x,position.y-i);
				for (int j = 0; j < tamanio; j++)
				{
					this.puntos.add(aux);
					aux = new Point(aux.x+1,aux.y);
				}
			}			
		}
	}
	
	public Ball createBall()
	{
		return new Ball();
	}
	public void DatafromBall(Ball ball, int mass, String name, int tamanio)
	{
		
		ball.fromBall( mass, name, tamanio);
	}
	public void DatafromLaunch(Ball ball,int angle , int gravity , Point position ,int velocity)
	{
		
		ball.fromLaunch( angle, gravity, position, velocity);
	}
	
	public Obstaculo createObstaculo(Point position , int tamanio)
	{
		return new Obstaculo( position ,  tamanio);
	}
	
	public Goal createGoal(Point position , int tamanio)
	{
		return new Goal( position ,  tamanio);
	}

}

package Backend;

import java.io.*;
import java.net.*;

import javax.xml.parsers.ParserConfigurationException;

import Backend.EntityFactory.Ball;
 /**
  * 
  * @author Base de Grupo 8
  *
  */
public class Client {
	public void send(Ball ball  , int original) throws IOException
	{
		int aQuien=((int)Math.random()*1000)%8;
		switch(aQuien)
		{
		case 0:
			sender("146.155.115.201",6969, ball,  original);
			break;
		case 1:
			sender("146.155.115.202", 1337,ball ,  original);
			break;
		case 2:
			sender("146.155.115.203", 15123, ball ,  original);
			break;
		case 3:
			sender("146.155.115.204",8080,ball ,  original);
			break;
		case 4:
			sender("146.155.115.205",1769, ball ,  original);
			break;
		case 5:
			//sender("146.155.115.206",puerto, posX ,  posY ,  width ,  height ,  original);
			break;
		case 6:
			sender("146.155.115.208",1234,ball,  original);
			break;
		case 7:
			sender("146.155.115.209",1234, ball ,  original);
			break;
		case 8:
			sender("146.155.115.210",1234, ball ,  original);
			break;
		
		}
	}
	
    public void sender(String ip , int puerto , Ball ball , int original) throws IOException {
 
        Socket socket = null;
        PrintWriter out = null;
        BufferedReader in = null;
 
        try {
            socket = new Socket(ip, puerto);
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host");
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection");
            System.exit(1);
        }
  
        XmlManager manager = new XmlManager();
        
        String xml="";
        try {
			xml = manager.writeXml(ball.position.x, ball.position.y, ball.tamanio, ball.tamanio ,  original);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Sending xml");
		out.println(xml);
		 
        out.close();
        in.close();
        socket.close();
    }
}
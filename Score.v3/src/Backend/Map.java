package Backend;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.Iterator;

import javax.swing.ImageIcon;

import pEventsUtil.pEventListener;
import Backend.EntityFactory.Obstaculo;
import Frontend.ElegirModo;

public class Map implements pEventListener{
	
	private ArrayList<Obstaculo> obstacles = new ArrayList<Obstaculo>();
	public double gravity;
	//TODO Una lista de pelotas
	
	public Map(){}
	
	
	public void setObstacle(Obstaculo obstaculo)
	{
		obstacles.add(obstaculo);
	}
	
	
	// Segun lo hablado con la profe esto esta obsoleto, hay q hacer un fix aqui
	public int[][] getObstaclesinRange(Point point, int height , int width)//Mas adelante este metodo chequea por pelotas tb
	{
		int[][] inRange=new int[height][width];//FilasxColumnas
		for(int i =0 ; i<inRange.length; i++)
		{
			for(int j=0; j<inRange[i].length ; j++ )
			{
				inRange[i][j]=0;
			}
		}
		Iterator<Obstaculo> itr=obstacles.iterator();
		
		
		while(itr.hasNext()) 
		{
			Obstaculo element = itr.next();
			if(element.position.y<=point.y-(height/2) && element.position.y>=point.y+(height/2))
			{
				if(element.position.x<=point.x-(width/2) && element.position.x>=point.x+(width/2))
				{
					inRange[element.position.y][element.position.x]=1;
				}
			}
	    }
		return inRange;
	}
	
	//Setear el mundo
	public void SetearMundo(int mundo)
	{
		//mundo Mercurio
		if(mundo == 1)
		{
			this.gravity = 3;
		}
		
		//mundo Tierra
		else if(mundo == 2)
		{
			this.gravity = 9.8;
		}
			 
		//mundo Jupiter
		else 
		{
			this.gravity = 23;
		}
		
	}

//Implemento el metodo que escuchara el evento
	@Override
	public void handleEvent(EventObject e, Object... params) {
		SetearMundo((Integer)params[0]);
		
	}


}

package scoreTest;

import static org.junit.Assert.*;
import Backend.EntityFactory;

import java.awt.*;

import org.junit.Test;

public class EntityFactoryTest {

	@Test
	public void CreateBalltest() {
		Backend.EntityFactory.Ball b = EntityFactory.Instance().createBall(1,1,1,"prueba",new Point(1,1),1,1);
		
		assertTrue(b.angle == 1 &&
				b.gravity == 1 &&
				b.mass == 1 &&
				b.name == "prueba" &&
				b.position.x == 1 && b.position.y == 1 &&
				b.tamanio == 1 &&
				b.velocity == 1);
	}
	
	@Test
	public void CreateObstaculotest()
	{
		Backend.EntityFactory.Obstaculo o = EntityFactory.Instance().createObstaculo(new Point(1,1), 1);
		assertTrue(o.position.x == 1 && o.position.y == 1 &&
				o.tamanio == 1);
	}
	
	@Test
	public void CreateGoaltest()
	{
		Backend.EntityFactory.Goal g = EntityFactory.Instance().createGoal(new Point(1,1), 1);
		assertTrue(g.position.x == 1 && g.position.y == 1 &&
				g.tamanio == 1);
	}

}


Aviso: Para compilar ant build
Crear el jar ant jar correr ant run

División Trabajo:

Pietro: Estructura Basica Backend Frontend, Creacion del paquete Event Utils, 
Recibo y Envio de Identidades, Uml, Jprofiler y Metrics.
Trabajó en el Ant.

Cristian: Corregir y terminar la Lógica del Juego (Backend en general). Hacer el método Simular() de la clase Juego del Backend, el cual tiene toda la lógica del lanzamiento de los objetos y su movimiento, choque, traspaso de paredes, ecuaciones físicas, coeficiente de fricción, etc. Clase Map y Nivel. Arreglo y Debugeo en General (Frontend y backend).

Isabelle: Corregir y terminar Frontend. Arreglos Y debugeo en general(Frontend y backend).
Interfaz gráfica: creación de varias pelotas, poder ver y modificar sus características.
Trabajo sobre la usabilidad (para evitar errores): no se puede lanzar o modificar pelotas si aún no se crea ninguna, control de los rangos de los datos ingresados por el usuario, no se puede modificar la gravedad en el modo competitivo.
Desarrollo del evento Click que permite relacionar el frontend y el backend en varias instancias.
Arregló los errores y bugs.

Diego: Correccion errores debugeo y JUnits
Arregló los errores que se desencadenaron en los tests JUnits.

El recibo y envio de entidades se hizo en conjunto con el grupo 8. Este grupo hizo el
 lector y escrito de xml. El gruo 8 hizo los sokets. Esto debido a q el codigo era 
comun y se optimizaba el tiempo. Igual se le hixieron cambios para que se adaptase a 
nuestras necesidades especificas